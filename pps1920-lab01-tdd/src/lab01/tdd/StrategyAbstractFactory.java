package lab01.tdd;

public interface StrategyAbstractFactory {

    SelectStrategy createEvenStrategy();

    SelectStrategy createMultipleOfStrategy(int elem);

    SelectStrategy createEqualsStrategy(int elem);

}
