package lab01.tdd;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    private List<Integer> list = new LinkedList<>();
    private int current = 0;

    @Override
    public void add(int element) {
        list.add(current, element);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if (list.isEmpty()) {
            return Optional.empty();
        }

        circularListIncrement();
        return Optional.of(list.get(current));
    }

    @Override
    public Optional<Integer> previous() {
        if (list.isEmpty()) {
            return Optional.empty();
        }

        current--;
        if (current < 0) {
            current = list.size() - 1;
        }

        return Optional.of(list.get(current));
    }

    @Override
    public void reset() {
        current = this.size() - 1;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if (list.isEmpty()) {
            return Optional.empty();
        }

        int stop = current;
        circularListIncrement();
        while (current != stop) {
            if (strategy.apply(list.get(current))) {
                return Optional.of(list.get(current));
            }
            circularListIncrement();
        }
        return Optional.empty();
    }

    private void circularListIncrement() {
        current = (current + 1) % list.size();
    }

}
