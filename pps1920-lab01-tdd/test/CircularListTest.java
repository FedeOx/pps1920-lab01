import lab01.tdd.CircularList;
import lab01.tdd.CircularListImpl;
import lab01.tdd.StrategyAbstractFactory;
import lab01.tdd.StrategyAbstractFactoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private static final int INSERT = 10;

    private CircularList list;
    private StrategyAbstractFactory factory = new StrategyAbstractFactoryImpl();

    @BeforeEach
    void beforeEach() {
        list = new CircularListImpl();
    }

    @Test
    void testListInitiallyEmpty() {
        assertTrue(list.isEmpty());
    }

    @Test
    void testAddElement() {
        assertTrue(list.isEmpty());
        list.add(1);
        assertFalse(list.isEmpty());
    }

    @Test
    void testAddMultipleElements() {
        for (int i=0; i<INSERT; i++) {
            list.add(1);
        }
        assertEquals(list.size(), INSERT);
    }

    @Test
    void testNextIsNull() {
        assertFalse(list.next().isPresent());
    }

    @Test
    void testNextIsCircular() {
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals(list.next().orElse(null), 2);
        assertEquals(list.next().orElse(null), 1);
        assertEquals(list.next().orElse(null), 3);
    }

    @Test
    void testPreviousIsNull() {
        assertFalse(list.previous().isPresent());
    }

    @Test
    void testPreviousIsCircular() {
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals(list.previous().orElse(null), 1);
        assertEquals(list.previous().orElse(null), 2);
        assertEquals(list.previous().orElse(null), 3);
    }

    @Test
    void testCurrentPointsToFirstOnReset() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.reset();
        assertEquals(list.next().orElse(null), 3);
        list.reset();
        assertEquals(list.previous().orElse(null), 2);
    }

    @Test
    void testEvenStrategy() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        assertEquals(list.next(factory.createEvenStrategy()).orElse(null), 2);
        assertEquals(list.next(factory.createEvenStrategy()).orElse(null), 4);
    }

    @Test
    void testEvenStrategyWithNoEven() {
        list.add(1);
        list.add(3);
        list.add(5);
        list.add(7);
        assertEquals(list.next(factory.createEvenStrategy()), Optional.empty());
    }

    @Test
    void testMultipleOfStrategy() {
        list.add(1);
        list.add(6);
        list.add(12);
        list.add(24);
        assertEquals(list.next(factory.createMultipleOfStrategy(6)).orElse(null), 12);
        assertEquals(list.next(factory.createMultipleOfStrategy(6)).orElse(null), 6);
    }

    @Test
    void testEqualsStrategy() {
        list.add(1);
        list.add(5);
        list.add(12);
        list.add(5);
        list.add(3);
        assertEquals(list.next(factory.createEqualsStrategy(5)).orElse(null), 5);
    }

}
