package lab01.example.model;

/**
 * This interface defines the concept of a bank account with ATM.
 */
public interface BankAccountWithAtm extends BankAccount {

    /**
     * Allows the deposit of an amount on the account applying the fee associated to this bank account, if the given
     * usrID corresponds to the register holder ID of the bank account. This ID acts like an "identification token".
     *
     * @param usrID the id of the user that wants do the deposit
     * @param amount the amount of the deposit
     */
    void depositWithATM(int usrID, double amount);

    /**
     * Allows the withdraw of an amount from the account applying the fee associated to this bank account, if the given
     * usrID corresponds to the register holder ID of the bank account. This ID acts like an "identification token" .
     *
     * @param usrID the id of the user that wants do the withdraw
     * @param amount the amount of the withdraw
     */
    void withdrawWithATM(int usrID, double amount);

}
