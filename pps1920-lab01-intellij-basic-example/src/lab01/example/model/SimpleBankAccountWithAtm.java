package lab01.example.model;

/**
 * This class represent a particular instance of a BankAccountWithAtm.
 * In particular, a Simple Bank Account allows always the deposit
 * while the withdraw is allowed only if the balance greater or equal the withdrawal amount.
 * Each transaction done with the ATM implies paying a certain amount of fees.
 */
public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    public interface TransactionFee {
         double fee(double amount);
    }

    private TransactionFee transactionFee;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance, TransactionFee transactionFee) {
        super(holder, balance);
        this.transactionFee = transactionFee;
    }

    public void depositWithATM(int usrID, double amount) {
        amount = transactionFee.fee(amount);
        super.deposit(usrID, amount);
    }

    public void withdrawWithATM(int usrID, double amount) {
        if (transactionFee.fee(getBalance()) - amount > 0) {
            super.withdraw(usrID, amount);
            balance = transactionFee.fee(getBalance());
        }
    }

}
